import math
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate 
from scipy.interpolate import CubicSpline 

#Tx = np.array([1,2,3,5])
#Ty = np.array([1,4,2,5])


def create_C(Tx,Ty):
    n = Tx.shape[0]-1
    C = np.empty([n+1,n+1], dtype = np.float64)
    for i in range(0,n+1):
        C[i][0] = Ty[i]
    for j in range(1,n+1):
        for i in range(j,n+1):
            C[i][j] = (C[i][j-1]-C[i-1][j-1])/(Tx[i]-Tx[i-j])
    return C


def calcul_P(C,x,Tx):
    n = Tx.shape[0]-1
    P = C[n][n]
    for i in range(n-1,-1,-1):
        #print(i)
        P = P*(x-Tx[i])+C[i][i]
    return P



"""
#test de l'interpolation par Newton
xplt.scatter(Tx,Ty, color = 'red', label = 'points réels')
plot = np.linspace (1, 5, 50)
yplot = np.array ([calcul_P(C,x) for x in xplot])
plt.plot (xplot, y, color = 'green', label = 'graphe')


#test par l'outil interp1d de scipy
f = interpolate.interp1d(Tx,Ty)
yplot = f(xplot) 
plt.plot (xplot, yplot, color = 'orange', label = 'graphe')
"""

#Exemple, Q7,Q8

xi = np.linspace (1,12,12)
yi = np.array([8.6,7,6.4,4,2.8,1.8,1.8,2.3,3.2,4.7,6.2,7.9])
C1 = create_C(xi,yi)
xinew = np.arange(1,12,0.1)
yinew = np.array ([calcul_P(C1,x,xi) for x in xinew])

#sans la valeur de Mars
xi2 = np.array([1,2,4,5,6,7,8,9,10,11,12])
yi2 = np.array([8.6,7,4,2.8,1.8,1.8,2.3,3.2,4.7,6.2,7.9])
C2 = create_C(xi2,yi2)
yinew2 = np.array ([calcul_P(C2,x,xi2) for x in xinew])


plt.plot (xinew, yinew,"--", color = 'orange', label = 'intpl avec mars')
plt.plot (xinew, yinew2,"--", color = 'blue', label = 'intpl sans mars')
plt.scatter(xi,yi, color = 'red', label = 'points réels')

#Outil splines cubiques
cs = CubicSpline(xi,yi)

plt.plot (xinew,cs(xinew), label = "S")


plt.legend()
plt.show()
