import math
import numpy as np
import matplotlib.pyplot as plt

Tx = np.array([1,2,3,5])
Ty = np.array([1,4,2,5])

def Neville(x):
    n=Tx.shape[0]-1
    P=np.empty([n+1,n+1], dtype=np.float64)
    for i in range(0,n+1):
         P[i][0]=Ty[i]
    for j in range(1,n+1):
        for i in range(j,n+1):
            P[i][j]=((x-Tx[i-j])*P[i][j-1] - (x-Tx[i])*P[i-1][j-1])/(Tx[i]-Tx[i-j])
    print(P)
    return P[3][3]
Neville(1)

#Affichage des points et des courbes

plt.scatter(Tx, Ty, color='red', label='courbe 1')



xplot = np.linspace (0.5, 7.5, 50)
yplot = np.array ([Neville(x) for x in xplot])
plt.plot (xplot, yplot, color='orange', label='graphe')

plt.legend()
plt.show()
