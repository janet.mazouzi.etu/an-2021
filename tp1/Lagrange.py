import math
import numpy as np
import matplotlib.pyplot as plt

"""def f(x):
	return (0.5*x+3.1)*x-1.05

def Lagrange() :
        abscisses = np.array([2,7,3,1])
        ordonnees = np.array([math.floor(f(x)) for x in abscisses])
        #plt.scatter(abscisses, ordonnees, color='red', label='courbe 1')

        xplot = np.linspace (0, 8, 50)
        yplot = np.array ([f(x) for x in xplot])
        plt.plot (xplot, yplot, color='orange', label='graphe')
        plt.legend()
        plt.show()
"""

Tx = np.array([2,7,3,1])
Ty = np.array([3,4,-1,2])


def denom (i) :
        # Tx désigne la variable globale Tx
        # Des variables locales n, result, k sont créées
        # Le symbole i désigne le paramètre formel
        n = Tx.shape[0]
        result = 1
        for k in range (0, n) :
                if i != k :
                        result *= Tx[i] - Tx[k]
        return result





# Retourne le produit des x - Tx[k] pour k = 0, 1, ..., i-1, i+1, ..., n
def numer (x, i) :
        n= Tx.shape[0]
        result = 1
        for k in range(0,n):
                if i!=k:
                        result *=x-Tx[k]
        return result

   
        
	
# Retourne Ty[i] * numer (x,i) / denom (i)
def Lagrange_L (x, i) :
      return Ty[i] * numer (x,i) / denom (i)


# Afficher les 4 points dont les coordonnées sont dans Tx et Ty.
plt.scatter(Tx, Ty, color='red', label='courbe 1')

# Superposer le graphe de Lagrange_L (x,3) pour 0.5 <= x <= 7.5

xplot = np.linspace (0.5, 7.5, 50)
yplot = np.array ([Lagrange_L(x,3) for x in xplot])
plt.plot (xplot, yplot, color='orange', label='graphe')
plt.legend()
plt.show()
