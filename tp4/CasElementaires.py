import math
import numpy as np
import matplotlib.pyplot as plt

A=np.array([[1,2,3],[0,1,2],[0,0,1]])
B=np.array([14,8,3])
C=np.array([[1,0,0],[1,2,0],[1,2,1]])


def ResolutionRemontee(A,B):
    n=A.shape[0]
    for i in range(0,n):
        if (A[i,i] == 0):
            return 0;
    X=np.empty([n,1],dtype = np.float64)
    X[n-1]=B[n-1]/A[n-1][n-1]

    for i in range(n-1,-1,-1):
        for j in range(i+1,n):
            B[i]=B[i]-A[i][j]*X[j]
        X[i]=B[i]/A[i][i]
    return X



#print(ResolutionRemontee(A,B))


def ResolutionDescente(C,B):

    n=C.shape[0]
    for i in range(0,n):
        if (C[i,i] == 0):
            return 0;
    X=np.empty([n,1],dtype = np.float64)
    X[1]=B[1]/C[1][1]

    for i in range(0,n):
        for j in range(i-1,-1,-1):
            B[i]=B[i]-C[i][j]*X[j]
        X[i]=B[i]/C[i][i]
    return X

print(ResolutionDescente(C,B))
