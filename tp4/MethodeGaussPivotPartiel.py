import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import lu

A = np.array([[1.,2.,3.],[0.,1.,2.],[0.,0.,1.]],dtype=np.float64)
B = np.array([15.,20.,8.])
C = np.array([[1.,0.,0.],[1.,2.,0.],[1.,2.,1.]],dtype=np.float64)
D = np.array([[1.,1.,4.],[1.,2.,5.],[1.,2.,1.]],dtype=np.float64)


def ResolutionRemontee(A,B):
    n = A.shape[0]
    for i in range(0,n):
        if (A[i,i] == 0):
            return 0;
    X = np.empty([n],dtype = np.float64)
    X[n-1] = B[n-1]/A[n-1][n-1]

    for i in range(n-1,-1,-1):
        for j in range(i+1,n):
            B[i]=B[i]-A[i][j]*X[j]
        X[i] = B[i]/A[i][i]
    return X


def ResolutionDescente(C,B):

    n = C.shape[0]
    for i in range(0,n):
        if (C[i,i] == 0):
            return 0;
    X = np.empty([n],dtype = np.float64)
    X[1] = B[1]/C[1][1]

    for i in range(0,n):
        for j in range(i-1,-1,-1):
            B[i] = B[i]-C[i][j]*X[j]
        X[i] = B[i]/C[i][i]
    return X


def GaussPivotPartiel(A,B):
    n = A.shape[0]
    for i in range(0,n-1):
        l = i
        for k in range(i,n):
            if(abs(A[k][i])>abs(A[l][i])):
                l = k

        if (l != i):
            for j in range(i,n+1):
                tmp = A[l][j]
                A[l][j] = A[i][j]
                A[i][j] = tmp
            tmp = B[l]
            B[l] = B[i]
            B[i] = tmp

        pivot = A[i][i]
        for k in range(i+1,n):
            factpivot = A[k][i]/pivot
            A[k][i] = 0
            for j in range(i+1,n):
                A[k][j] = A[k][j] - factpivot*A[i][j]
            B[k] =  B[k] - factpivot * B[i]
    return ResolutionRemontee(A,B)


def Cholesky(A,B):
	n = A.shape[0]
	S1 = 0
	S2 = 0
	L = np.empty([n,n],dtype = np.float64)
	L[0][0] = math.sqrt(A[0][0])
	for i in range(1,n):
		L[i][0] = A[i][0]/L[0][0]
	
	for j in range(1,n):
		for i in range(0,j):
			L[i][j] = 0
			S1 = S1 + (L[j][i])**2
		L[j][j] = math.sqrt((A[j][j]-S1))
		S1 = 0
		for i in range(j+1,n):
			for k in range(1,n):
				S2 = S2 + L[i,k]*L[j,k]
			L[i][j] = (A[i][j] - S2)/L[j][j]
			S2 = 0
	return L







#print(GaussPivotPartiel(D,B))

'''
A = np.array([[2.,4.,4.],[1.,3.,1.],[1.,5.,6.]],dtype=np.float64)
B = np.array([2.,1.,-6.])

[E,L,U]=lu(A)
#print(E)
print(np.transpose(E))
print(B)
C = np.multiply(np.transpose(E),B)
#Y = ResolutionDescente(L,np.transpose(E)*B)
print(C)
#print(ResolutionRemontee(U,Y))
'''


A = np.array([[9.,6.,3.],[6.,20.,6.],[3.,6.,3.]],dtype=np.float64)
B = np.array([39.,86.,27.])

print(Cholesky(A,B))
L=Cholesky(A,B)
Bb = ResolutionDescente(L,B)
X = ResolutionRemontee(np.transpose(L),Bb)

print(X)

