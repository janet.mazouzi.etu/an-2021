import math
import numpy as np
import matplotlib.pyplot as plt



def relerr(I,Ip) :
    return abs(I-Ip)/abs(I)


def nbbits(I,Ip):
    return -(math.log2(relerr(I,Ip)))


def f(x) :
    return (-(math.sin(math.log(x))/(x*(1+x*x)))-(2*math.cos(math.log(x))*x)/((1+x*x)**2))


def p(x) :
    return math.cos(math.log(x))/(1+x*x)




def trapeze(y,h):
    n=y.shape[0]-1
    integrale = (y[0]+y[n])/2
    for i in range(1,n):
        integrale=integrale + y[i]
    return h*integrale 



def simpson(y,h):
    n = y.shape[0]-1
    integrale = y[0]+y[n]
    for i in range(1,n):
        if(i%2 == 0):
            integrale = integrale+2*y[i]
        else:
            integrale = integrale+4*y[i]

    return h*integrale/3 

def simpsonImpaire(y,h):
    n = y.shape[0]-1
    return simpson(y,(x[n-3]-x[0])/(n-3)) + trapeze(y,x[n]-x[n-2]

    





print(trapeze(np.array([0,1,2,3]),1))
print(simpson(np.array([0,1,4,9,16]),1))



a = 2
b = 12
#h = 2
I = p(b)-p(a)
#x = np.linspace(2,12,6)
#y = np.array ([f(t) for t in x])
#Ip = trapeze(y,h)


#print("I:"+str(I)+" I':"+str(Ip))
#print("erreur relative:"+str(relerr(I,Ip)))
#print(str(nbbits(I,Ip)))
print("\n")


#Affichage du tableau et rentré des valeurs pour les tableaux d'affichage des graphs

Tx = np.zeros(15)
TyT = np.zeros(15)
TyS = np.zeros(15)

#print("k    n        nbbitsTrap           nbbitsSimp")
print("k    n     pTrap        pSimp ")
for k in range(2,17):
    n=2**k
    h=(b-a)/n
    x = np.linspace(a,b,n+1)
    y = np.array ([f(t) for t in x])
    Ip1 = trapeze(y,h)
    Ip2 = simpson(y,h)
    #print(str(k)+"    "+str(n)+"    "+str(math.log(h)/math.log(relerr(I,Ip1)))+"    "+ str(math.log(h)/math.log(relerr(I,Ip2))))
    #print(str(k)+"    "+str(n)+"     "+str(nbbits(I,Ip1))+"     "+str(nbbits(I,Ip2)))
    Tx[k-2] = k
    TyT[k-2] = nbbits(I,Ip1)
    TyS[k-2] = nbbits(I,Ip2) 

print("\n") 

#Affichage des graphes 

#print(str(2.459323-0.62509))
#print(str(16.4041375-14.404145))   
plt.scatter(Tx,TyT, color = "red", label = "nbbits trapèze")
plt.scatter(Tx,TyS, color = "blue", label = "nbbits simpson")


"""
#affichage de f
xtest=np.linspace(1,15,15)
ytest=np.array([f(x) for x in xtest])
plt.scatter(xtest,ytest, color = "red", label = "f")
"""
plt.legend()
plt.show()

    
    
              
