Travaux pratiques d'Analyse Numérique en SE3 - Groupe 1

Ce projet comporte quatre répertoires correspondant à quatre TP
Certains TP peuvent se dérouler sur plusieurs séances

Le langage utilisé est Python 3.

Pour chaque TP, un compte-rendu est demandé sous la forme d'un ou
plusieurs fichiers. Le rendu se fera en utilisant git.
